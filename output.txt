[0KRunning with gitlab-runner 15.1.0 (76984217)[0;m
[0K  on a _wyXoy9N[0;m
section_start:1658754061:prepare_executor
[0K[0K[36;1mPreparing the "shell" executor[0;m[0;m
[0KUsing Shell executor...[0;m
section_end:1658754061:prepare_executor
[0Ksection_start:1658754061:prepare_script
[0K[0K[36;1mPreparing environment[0;m[0;m
Running on ip-172-31-24-195.ap-southeast-1.compute.internal...
section_end:1658754061:prepare_script
[0Ksection_start:1658754061:get_sources
[0K[0K[36;1mGetting source from Git repository[0;m[0;m
[32;1mFetching changes with git depth set to 20...[0;m
Reinitialized existing Git repository in /home/gitlab-runner/builds/_wyXoy9N/0/root/aem_with_multiple_pom/.git/
[32;1mChecking out da59c347 as main...[0;m
[32;1mSkipping Git submodules setup[0;m
section_end:1658754062:get_sources
[0Ksection_start:1658754062:step_script
[0K[0K[36;1mExecuting "step_script" stage of the job script[0;m[0;m
[32;1m$ git config --global user.name "root"[0;m
[32;1m$ git config --global user.email "kunalchoudharkar1995@gmail.com"[0;m
[32;1m$ git checkout main[0;m
Switched to branch 'main'
Your branch is up to date with 'origin/main'.
[32;1m$ sudo git pull "http://root:12345678@ec2-13-214-53-238.ap-southeast-1.compute.amazonaws.com/root/aem_with_multiple_pom.git"[0;m
From http://ec2-13-214-53-238.ap-southeast-1.compute.amazonaws.com/root/aem_with_multiple_pom
 * branch            HEAD       -> FETCH_HEAD
Already up to date.
[32;1m$ sudo chown -R gitlab-runner:gitlab-runner /home/gitlab-runner/builds/_wyXoy9N/0/root/aem_with_multiple_pom/[0;m
[32;1m$ old_version=$(grep -oPm1 "(?<=<version>)[^<]+" "pom.xml")[0;m
[32;1m$ echo "$old_version"[0;m
2.0.21-SNAPSHOT
[32;1m$ sudo echo 'Y' | mvn release:update-versions -DautoVersionSubmodules=true[0;m
[INFO] Scanning for projects...
[INFO] ------------------------------------------------------------------------
[INFO] Reactor Build Order:
[INFO] 
[INFO] WKND Sites Project - Reactor Project
[INFO] WKND Sites Project - Core
[INFO] WKND Sites Project - UI Frontend
[INFO] WKND Sites Project - UI apps structure
[INFO] WKND Sites Project - UI apps
[INFO] WKND Sites Project - UI content
[INFO] WKND Sites Project - UI config
[INFO] WKND Sites Project - UI sample content
[INFO] WKND Sites Project - All
[INFO] WKND Sites Project - Integration Tests
[INFO] WKND Sites Project - Dispatcher
[INFO] WKND Sites Project - UI Tests
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building WKND Sites Project - Reactor Project 2.0.21-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-release-plugin:3.0.0-M5:update-versions (default-cli) @ aem-guides-wknd ---
[INFO] phase verify-release-configuration
[INFO] starting updateVersions goal, composed of 4 phases: check-poms-updateversions, create-backup-poms, map-development-versions, rewrite-pom-versions
[INFO] [updateVersions] 1/4 check-poms-updateversions
[INFO] [updateVersions] 2/4 create-backup-poms
[INFO] [updateVersions] 3/4 map-development-versions
What is the new development version for "WKND Sites Project - Reactor Project"? (com.adobe.aem.guides:aem-guides-wknd) 2.0.23-SNAPSHOT: : What is the new development version for "WKND Sites Project - Reactor Project"? (com.adobe.aem.guides:aem-guides-wknd) 2.0.23-SNAPSHOT: : [INFO] [updateVersions] 4/4 rewrite-pom-versions
[INFO] Transforming 'WKND Sites Project - Reactor Project'...
[INFO] Transforming 'WKND Sites Project - Core'...
[INFO] Transforming 'WKND Sites Project - UI Frontend'...
[INFO] Transforming 'WKND Sites Project - UI apps structure'...
[INFO] Transforming 'WKND Sites Project - UI apps'...
[INFO]   Ignoring artifact version update for expression ${project.version}
[INFO]   Ignoring artifact version update for expression ${project.version}
[INFO]   Ignoring artifact version update for expression ${project.version}
[INFO] Transforming 'WKND Sites Project - UI content'...
[INFO]   Ignoring artifact version update for expression ${project.version}
[INFO]   Ignoring artifact version update for expression ${project.version}
[INFO] Transforming 'WKND Sites Project - UI config'...
[INFO]   Ignoring artifact version update for expression ${project.version}
[INFO] Transforming 'WKND Sites Project - UI sample content'...
[INFO]   Ignoring artifact version update for expression ${project.version}
[INFO]   Ignoring artifact version update for expression ${project.version}
[INFO]   Ignoring artifact version update for expression ${project.version}
[INFO] Transforming 'WKND Sites Project - All'...
[INFO]   Ignoring artifact version update for expression ${project.version}
[INFO]   Ignoring artifact version update for expression ${project.version}
[INFO]   Ignoring artifact version update for expression ${project.version}
[INFO]   Ignoring artifact version update for expression ${project.version}
[INFO] Transforming 'WKND Sites Project - Integration Tests'...
[INFO] Transforming 'WKND Sites Project - Dispatcher'...
[INFO] Transforming 'WKND Sites Project - UI Tests'...
[INFO] phase cleanup
[INFO] Cleaning up after release...
[INFO] ------------------------------------------------------------------------
[INFO] Reactor Summary:
[INFO] 
[INFO] WKND Sites Project - Reactor Project .............. SUCCESS [1.275s]
[INFO] WKND Sites Project - Core ......................... SKIPPED
[INFO] WKND Sites Project - UI Frontend .................. SKIPPED
[INFO] WKND Sites Project - UI apps structure ............ SKIPPED
[INFO] WKND Sites Project - UI apps ...................... SKIPPED
[INFO] WKND Sites Project - UI content ................... SKIPPED
[INFO] WKND Sites Project - UI config .................... SKIPPED
[INFO] WKND Sites Project - UI sample content ............ SKIPPED
[INFO] WKND Sites Project - All .......................... SKIPPED
[INFO] WKND Sites Project - Integration Tests ............ SKIPPED
[INFO] WKND Sites Project - Dispatcher ................... SKIPPED
[INFO] WKND Sites Project - UI Tests ..................... SKIPPED
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 34.611s
[INFO] Finished at: Mon Jul 25 13:01:39 UTC 2022
[INFO] Final Memory: 24M/167M
[INFO] ------------------------------------------------------------------------
[32;1m$ sudo echo '[http] postbuffer = 5m'.git/config[0;m
[http] postbuffer = 5m.git/config
[32;1m$ new_version=$(grep -oPm1 "(?<=<version>)[^<]+" "pom.xml")[0;m
[32;1m$ echo "$new_version"[0;m
2.0.23-SNAPSHOT
[32;1m$ git add .[0;m
[32;1m$ git commit -m "new2"[0;m
[main d158870] new2
 12 files changed, 12 insertions(+), 12 deletions(-)
[32;1m$ first_name="release/"[0;m
[32;1m$ final_output=$first_name$new_version[0;m
[32;1m$ if [ "$new_version" == "$old_version" ]; then echo "Nothing to do";else echo "$final_output"; fi[0;m
release/2.0.23-SNAPSHOT
[32;1m$ new_branch= git checkout -b $final_output[0;m
Switched to a new branch 'release/2.0.23-SNAPSHOT'
[32;1m$ git push -o merge_request.create -o merge_request.label="test_25_07 to main" -o merge_request.target=main http://root:12345678@ec2-13-214-53-238.ap-southeast-1.compute.amazonaws.com/root/aem_with_multiple_pom.git[0;m
remote: 
remote: View merge request for release/2.0.23-SNAPSHOT:        
remote:   http://ec2-13-214-53-238.ap-southeast-1.compute.amazonaws.com/root/aem_with_multiple_pom/-/merge_requests/7        
Deployment Completed
remote: 
To http://ec2-13-214-53-238.ap-southeast-1.compute.amazonaws.com/root/aem_with_multiple_pom.git
 * [new branch]      release/2.0.23-SNAPSHOT -> release/2.0.23-SNAPSHOT
section_end:1658754102:step_script
[0K[32;1mJob succeeded[0;m
